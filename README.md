# README #
CI Library FakeData

### Library Apa ini ? ###

* Berfungsi untuk mempopulasi data data dummy, guna kebutuhan pemenuhan konten jika dibutuhkan.

### Setup? ###

Salinfile fakedata.php kedalam application/libraries/
*oad pada controller dibutuhkan dengan cara
```
#!php

$this->load->library('fakedata');
```


### Penggunaan ###

Dalam Library ini terdapat 11 data palsu atau dummy yang dapat dihasilkan, diantaranya :

1. First Name
*Cara Pakai
    
```
#!php

$name = $this->fakedata->first_name();
```
*Contoh Output = Bacher.

2.Last Name
*Cara Pakai
    
```
#!php

$name = $this->fakedata->last_name();
```
*Contoh Output = Aasen.

3.User Name

*Cara Pakai
    
```
#!php

$name = $this->fakedata->username();
```
*Contoh Output = bacher_aasen.
4.Email

```
#!php

$email= $this->fakedata->email();
```
*Contoh Output = bacher_aasen@fakemail.info
5.Password
```
#!php

$password= $this->fakedata->password();
```
*Contoh Output = nF98Uo4xZzvf33FG
6.Telephone
```
#!php

$phone= $this->fakedata->phone();
```
*Cotoh Output = (128) 2685877
7.Freetext

```
#!php

$text= $this->fakedata->freetext();
```
atau jika ingin dibatasi karakternya

```
#!php

$text= $this->fakedata->freetext(150);
```

*Contoh Output = Lorem ipsum dolor sit amet .....
8.City

```
#!php

$city= $this->fakedata->city();
```

*Contoh Output = Bandung
9.Address

```
#!php

$address= $this->fakedata->address();
```

*Contoh Output = Jl Ir Juanda, 243, Bandung
10.Number

```
#!php

$number= $this->fakedata->number();
```
*Contoh Output = Jl Ir Juanda, 243, Bandung
(menghasilkan random number dari 0-99999999
11.Postal Code


```
#!php

$postalcode= $this->fakedata->postalcode();
```
*Contoh Output =40243

### Ingin Lebih Detail ? ###

*http://www.semacamblog.com/