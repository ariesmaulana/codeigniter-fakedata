<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fakedata
{
    /*
    Januari-2015
    Author : Aries Maulana Permana
    Email  : ariesmaulana7@gmail.com
    Site   : www.semacamblog.com
    
    Sekilas tentang library
    Libary ini berfungsi untuk menghasilkan beberapa data dummy, biasa digunakan untuk mengisi konten
    seperti nama,alamat,email,telepon dan sebagainya.
    
    Dapat digunakan ketika dibutuhkan untuk mempopulasi data dummy sehingga diharapkan user tidak perlu input manual dan cukup
    menjalankan beberapa baris perintah sehingga data dummy bisa langsung dikeluarkan.
    */
    public function __construct()
    {
        
        $Fake =& get_instance();
        $Fake->load->helper('string');
        $Fake->load->helper('array');
        $Fake->load->helper('number');
        
    }
    
    var $firstname = null;
    var $lastname = null;
    var $username = null;
    var $cities = null;
    
    
    public function first_name()
    {
        $firstname       = array(
            "Reed",
            "Middlebrook",
            "Mickie",
            "Wasko",
            "Aisha",
            "Thiry",
            "Lino",
            "Haviland",
            "Bula",
            "Ancona",
            "Tangela",
            "Ayala",
            "Eleanor",
            "Carolan",
            "Cori",
            "Millar",
            "Earnest",
            "Laughridge",
            "Myesha",
            "Kleinschmidt",
            "Danielle",
            "Whalen",
            "Keri",
            "Lamphear",
            "Dirk",
            "Zahl",
            "Rusty",
            "Steere",
            "Keli",
            "Mcclinton",
            "Tena",
            "Krier",
            "Isaiah",
            "Bacher",
            "Taunya",
            "Aasen",
            "Dominick",
            "Stegman",
            "Annabell",
            "Foushee"
        );
        $this->firstname = random_element($firstname);
        return $this->firstname;
    }
    
    public function last_name()
    {
        $lastname = array(
            "Reed",
            "Middlebrook",
            "Mickie",
            "Wasko",
            "Aisha",
            "Thiry",
            "Lino",
            "Haviland",
            "Bula",
            "Ancona",
            "Tangela",
            "Ayala",
            "Eleanor",
            "Carolan",
            "Cori",
            "Millar",
            "Earnest",
            "Laughridge",
            "Myesha",
            "Kleinschmidt",
            "Danielle",
            "Whalen",
            "Keri",
            "Lamphear",
            "Dirk",
            "Zahl",
            "Rusty",
            "Steere",
            "Keli",
            "Mcclinton",
            "Tena",
            "Krier",
            "Isaiah",
            "Bacher",
            "Taunya",
            "Aasen",
            "Dominick",
            "Stegman",
            "Annabell",
            "Foushee"
        );
        
        $this->lastname = random_element($lastname);
        return $this->lastname;
    }
    
    public function username()
    {
        $username = array(
            "Reed",
            "Middlebrook",
            "Mickie",
            "Wasko",
            "Aisha",
            "Thiry",
            "Lino",
            "Haviland",
            "Bula",
            "Ancona",
            "Tangela",
            "Ayala",
            "Eleanor",
            "Carolan",
            "Cori",
            "Millar",
            "Earnest",
            "Laughridge",
            "Myesha",
            "Kleinschmidt",
            "Danielle",
            "Whalen",
            "Keri",
            "Lamphear",
            "Dirk",
            "Zahl",
            "Rusty",
            "Steere",
            "Keli",
            "Mcclinton",
            "Tena",
            "Krier",
            "Isaiah",
            "Bacher",
            "Taunya",
            "Aasen",
            "Dominick",
            "Stegman",
            "Annabell",
            "Foushee"
        );
        
        if ($this->firstname == null and $this->lastname == null) {
            $username = strtolower(random_element($username));
        } elseif ($this->firstname != null and $this->lastname == null) {
            $username = strtolower($this->firstname);
        } elseif ($this->firstname == null and $this->lastname != null) {
            $username = strtolower($this->lastname);
        } else {
            $username = strtolower($this->firstname . '_' . $this->lastname);
        }
        return $username;
    }
    
    public function email()
    {
        $domains  = array(
            "com",
            "net",
            "gov",
            "org",
            "edu",
            "biz",
            "info"
        );
        $provider = array(
            "gmail",
            "yahoo",
            "mymail",
            "myweb",
            "fakemail"
        );
        $listname = array(
            "Reed",
            "Middlebrook",
            "Mickie",
            "Wasko",
            "Aisha",
            "Thiry",
            "Lino",
            "Haviland",
            "Bula",
            "Ancona",
            "Tangela",
            "Ayala",
            "Eleanor",
            "Carolan",
            "Cori",
            "Millar",
            "Earnest",
            "Laughridge",
            "Myesha",
            "Kleinschmidt",
            "Danielle",
            "Whalen",
            "Keri",
            "Lamphear",
            "Dirk",
            "Zahl",
            "Rusty",
            "Steere",
            "Keli",
            "Mcclinton",
            "Tena",
            "Krier",
            "Isaiah",
            "Bacher",
            "Taunya",
            "Aasen",
            "Dominick",
            "Stegman",
            "Annabell",
            "Foushee"
        );
        if ($this->firstname == null and $this->lastname == null) {
            $name = strtolower(random_element($listname));
        } elseif ($this->firstname != null and $this->lastname == null) {
            $name = strtolower($this->firstname);
        } elseif ($this->firstname == null and $this->lastname != null) {
            $name = strtolower($this->lastname);
        } else {
            $name = strtolower($this->firstname . '_' . $this->lastname);
        }
        $email = $name . "@" . random_element($provider) . "." . random_element($domains);
        return $email;
    }
    
    function password()
    {
        
        return random_string('alnum', 16);
    }
    public function phone()
    {
        $area  = substr(number_format(time() * mt_rand(), 0, '', ''), 0, 3);
        $phone = substr(number_format(time() * mt_rand(), 0, '', ''), 0, 7);
        
        $phonenumber = "(" . $area . ") " . $phone;
        return $phonenumber;
    }
    
    
    
    public function freetext($limit = "")
    {
        $text = "
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vitae odio ut nisi dictum tempus. Pellentesque viverra, metus vitae lobortis molestie, arcu leo ullamcorper elit, commodo fermentum magna sapien vel tortor. Suspendisse potenti. Phasellus ac lorem ac nunc pretium venenatis a quis est. Donec lacus lacus, suscipit vel lacus quis, eleifend malesuada lectus. Praesent volutpat dui dapibus ullamcorper tempus. Sed accumsan scelerisque ante in tristique. Cras sagittis finibus mi, vel tincidunt est ultricies id. Proin varius justo sed elementum eleifend. Aliquam turpis nisi, volutpat eu vehicula nec, bibendum ac leo. Nulla facilisi. Phasellus ut dictum elit, eu finibus dolor. Pellentesque nisi diam, mattis a ullamcorper sit amet, egestas vel diam. Nam dictum efficitur nisi, eu gravida risus volutpat in. Ut lobortis, ipsum eu vehicula egestas, urna felis aliquam sem, nec varius ligula nisl at ante.

Vivamus posuere rutrum ex, sed interdum arcu luctus et. Sed nec iaculis magna. Suspendisse orci tellus, tempor ut urna vel, euismod semper odio. Maecenas gravida condimentum consectetur. Maecenas eros urna, condimentum ut ornare a, elementum sit amet neque. Sed ornare risus ac felis suscipit, eget ornare mauris consectetur. Curabitur id justo egestas elit tempus efficitur.

Morbi ac libero a urna tempor rutrum at eu nulla. Ut vitae imperdiet sem. Cras cursus diam a pulvinar efficitur. Pellentesque porttitor aliquet mauris, ut porta lacus blandit bibendum. Mauris convallis magna sit amet felis interdum commodo. Sed viverra, mi quis pretium varius, arcu diam lacinia ex, eu vulputate eros lectus sit amet ante. Nullam iaculis eleifend enim sit amet facilisis. Fusce at arcu quis erat ultricies commodo. Nullam eu fringilla mauris. Duis eu erat eget magna iaculis tristique. Suspendisse id nisl egestas, sollicitudin justo nec, aliquet tortor. Sed venenatis vehicula quam quis lobortis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pharetra arcu vel facilisis suscipit.

Curabitur eu vehicula nisi. Nullam pretium, justo id sodales imperdiet, mi mi rutrum justo, non sodales lectus nisi nec arcu. Ut quis urna vel libero ultricies fringilla. Ut id diam a lorem euismod cursus. Vestibulum fermentum lobortis quam posuere aliquet. Proin ac ultrices nulla. Integer vel ligula suscipit, porttitor magna sit amet, semper lorem. Aliquam erat volutpat. Proin vulputate dignissim condimentum. Cras quis vehicula risus. Nam varius vel libero id accumsan.

Suspendisse ut massa eu nibh faucibus porttitor ac non sapien. Mauris sagittis diam sed nisi commodo, ac aliquet nisi euismod. In cursus lacinia nulla in porta. Ut tempus fringilla ligula hendrerit pulvinar. Praesent quis varius lacus. Integer commodo urna tortor, interdum tempus tellus hendrerit vitae. Donec sed auctor velit. Etiam eget bibendum nulla. In hac habitasse platea dictumst. Cras nec turpis efficitur, sodales mi in, cursus ante. Etiam eu est imperdiet, ornare mi lobortis, tincidunt purus. Proin luctus elit id arcu malesuada ornare. ";
        
        
        return substr($text, 0,(($limit == "") ? "500": $limit));
        
    }
    function city()
    {
        $city = array(
            'Jakarta',
            'Bandung',
            'Surabaya',
            'Makasar',
            'Medan',
            'Palembang',
            'Malang',
            'Jayapura',
            'Wamena',
            'Denpasar'
        );
        
        $this->cities = random_element($city);
        
        return $this->cities;
    }
    function address()
    {
        $street = array(
            'Jl Jendral Sudirman',
            'Jl Diponegoro',
            'Jl Gatot Subroto',
            'Jl Ir Juanda',
            'Jl Achmad Yani',
            'Jl R.E Martadinata',
            'Jl Kapt Tendean',
            'Jl Daan Mogot',
            'Jl M.T Haryono',
            'Jl Otto Iskandardinata'
        );
        $number = rand(0, 999);
        
        if ($this->cities == "") {
            $city = $this->city();
        } else {
            $city = $this->cities;
        }
        $address = random_element($street) . ', ' . $number . ', ' . $city;
        
        return $address;
    }
    
    public function number()
    {
        $number = rand(0, 99999999);
        return $number;
    }

     public function postalcode()
    {
        $number = rand(1000, 9999);
        return $number;
    }
}
